from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to allow only owners of an object to modify it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions allowed for any request, so always allow: GET, HEAD, OPTIONS
        if request.method in permissions.SAFE_METHODS:
            return True

        # write permissions allowed only for owner of a snippet
        return obj.owner == request.user
